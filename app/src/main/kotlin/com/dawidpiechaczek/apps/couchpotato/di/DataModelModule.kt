package com.dawidpiechaczek.apps.couchpotato.di

import com.dawidpiechaczek.apps.couchpotato.db.CouchDatabase
import com.dawidpiechaczek.apps.couchpotato.db.FilmsRepository
import com.dawidpiechaczek.apps.couchpotato.data.MainDataModel
import org.koin.dsl.module

val dataModelModule = module {
    single { get<CouchDatabase>().filmDao() }
    single { get<CouchDatabase>().directorsDao() }
    single { get<CouchDatabase>().reviewDao() }
    single { MainDataModel(get(), get(), get()) as FilmsRepository }
}