package com.dawidpiechaczek.apps.couchpotato.mapper

import com.dawidpiechaczek.apps.couchpotato.db.entity.ReviewEntity
import com.dawidpiechaczek.apps.couchpotato.model.Review

object ReviewEntityToReview {
    fun from(reviewEntity: ReviewEntity) =
        Review(
            reviewEntity.idFilm,
            reviewEntity.mark
        )
}