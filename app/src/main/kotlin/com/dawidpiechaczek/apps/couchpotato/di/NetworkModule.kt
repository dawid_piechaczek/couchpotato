package com.dawidpiechaczek.apps.couchpotato.di

import com.dawidpiechaczek.apps.couchpotato.service.FilmService
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    single { filmService }
}

private val SERVER_URL = "http://couchpotatos.azurewebsites.net/api/"

private val filmService: FilmService =
    createWebService(
        createOkHttpClient(),
        SERVER_URL
    )

private fun createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addNetworkInterceptor(StethoInterceptor())
        .build()
}

fun createWebService(okHttpClient: OkHttpClient, url: String): FilmService {
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(FilmService::class.java)
}


