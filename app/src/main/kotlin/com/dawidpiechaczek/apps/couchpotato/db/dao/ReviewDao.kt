package com.dawidpiechaczek.apps.couchpotato.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dawidpiechaczek.apps.couchpotato.db.entity.ReviewEntity
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface ReviewDao {

    @Query(GET_ALL_REVIEWS)
    fun getAll(): List<ReviewEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(film: ReviewEntity)

    @Query(DELETE_ALL)
    fun deleteAll()

    companion object {
        const val GET_ALL_REVIEWS: String = "SELECT * FROM reviews"
        const val DELETE_ALL: String = "DELETE FROM reviews"
    }
}