package com.dawidpiechaczek.apps.couchpotato.ui.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel: ViewModel(){
    private val disposables = CompositeDisposable()

    fun addDisposable(disposable: Disposable){
        disposables.addAll(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}