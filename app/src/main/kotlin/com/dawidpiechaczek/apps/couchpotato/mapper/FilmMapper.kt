package com.dawidpiechaczek.apps.couchpotato.mapper

import com.dawidpiechaczek.apps.couchpotato.db.entity.FilmEntity
import com.dawidpiechaczek.apps.couchpotato.model.Film
import com.dawidpiechaczek.apps.couchpotato.model.FilmDTO

object FilmDTOToFilmEntity {
    fun from(filmDTO: FilmDTO) =
        FilmEntity(
            filmDTO.id_film,
            filmDTO.id_director,
            filmDTO.title_pl,
            filmDTO.title_orgin,
            filmDTO.foto_mini,
            filmDTO.foto,
            filmDTO.poster_mini,
            filmDTO.poster,
            filmDTO.duration,
            filmDTO.year_production,
            filmDTO.description_film,
            filmDTO.grade,
            filmDTO.motto,
            filmDTO.popularity,
            filmDTO.date_add,
            filmDTO.amount_grades,
            filmDTO.category
        )
}

object FilmEntityToFilm {
    fun from(filmEntity: FilmEntity) =
        Film(
            filmEntity.id_film,
            filmEntity.id_director,
            filmEntity.title_pl,
            filmEntity.poster,
            filmEntity.year_production,
            filmEntity.description_film,
            filmEntity.category
        )
}
