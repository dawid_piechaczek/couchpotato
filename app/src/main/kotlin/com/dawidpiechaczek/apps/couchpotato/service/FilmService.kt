package com.dawidpiechaczek.apps.couchpotato.service

import com.dawidpiechaczek.apps.couchpotato.model.DirectorDTO
import com.dawidpiechaczek.apps.couchpotato.model.FilmDTO
import com.dawidpiechaczek.apps.couchpotato.model.FilmIds
import com.dawidpiechaczek.apps.couchpotato.model.SimilarityDTO
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface FilmService {
    @GET("films")
    fun fetchFilms(): Single<List<FilmDTO>>

    @POST("films/getSimilarFilms")
    fun fetchSimilarFilms(@Body filmIds: FilmIds): Single<List<SimilarityDTO>>

    @GET("directors")
    fun fetchDirectors(): Single<List<DirectorDTO>>
}