package com.dawidpiechaczek.apps.couchpotato.di

import androidx.room.Room
import com.dawidpiechaczek.apps.couchpotato.db.CouchDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            CouchDatabase::class.java,
            "couch-db"
        )
            .build()
    }
}