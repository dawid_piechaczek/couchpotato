package com.dawidpiechaczek.apps.couchpotato.model

data class Director(
    val id_director: Long,
    val name_director: String
)