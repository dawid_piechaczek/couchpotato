package com.dawidpiechaczek.apps.couchpotato.model

class FilmIds(
    val filmIds: List<Long>
)