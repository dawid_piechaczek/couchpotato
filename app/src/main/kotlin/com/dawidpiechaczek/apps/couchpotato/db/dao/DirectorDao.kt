package com.dawidpiechaczek.apps.couchpotato.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dawidpiechaczek.apps.couchpotato.db.entity.DirectorEntity
import io.reactivex.Single

@Dao
interface DirectorDao {

    @Query(GET_ALL_REVIEWS)
    fun getDirectorById(id: Long): Single<DirectorEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(film: List<DirectorEntity>)

    companion object {
        const val GET_ALL_REVIEWS: String = "SELECT * FROM directors WHERE id_director == :id"
    }
}