package com.dawidpiechaczek.apps.couchpotato.ui.pager.summary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.dawidpiechaczek.apps.couchpotato.R
import com.dawidpiechaczek.apps.couchpotato.adapter.RecommendAdapter
import com.dawidpiechaczek.apps.couchpotato.utils.RxSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_summary.view.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class SummaryFragment : Fragment() {
    private val summaryFragmentViewModel: SummaryFragmentViewModel by viewModel()
    private val rxSchedulers: RxSchedulers by inject()
    private val disposables = CompositeDisposable()
    private val recommendationAdapter = RecommendAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_summary, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        summaryFragmentViewModel.getReviewsForFilms()

        disposables.add(summaryFragmentViewModel.getUiEventsPublishSubject()
            .observeOn(rxSchedulers.ui)
            .subscribeOn(rxSchedulers.network)
            .subscribe(
                {
                    recommendationAdapter.setFilms(it)
                },
                {
                    Timber.e(it)
                }
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.recommended_films.layoutManager = GridLayoutManager(context, 2)
        view.recommended_films.adapter = recommendationAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    companion object {
        fun newInstance(): SummaryFragment {
            return SummaryFragment()
        }
    }
}