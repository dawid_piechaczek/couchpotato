package com.dawidpiechaczek.apps.couchpotato.mapper

import com.dawidpiechaczek.apps.couchpotato.db.entity.DirectorEntity
import com.dawidpiechaczek.apps.couchpotato.model.Director
import com.dawidpiechaczek.apps.couchpotato.model.DirectorDTO

object DirectorDTOToDirectorEntity {
    fun from(directorDTO: DirectorDTO) =
        DirectorEntity(
            directorDTO.id_director,
            directorDTO.name_director
        )
}

object DirectorEntityToDirector {
    fun from(directorEntity: DirectorEntity) =
        Director(
            directorEntity.idDirector,
            directorEntity.nameDirector
        )
}