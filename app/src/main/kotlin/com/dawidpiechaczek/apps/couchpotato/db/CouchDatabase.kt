package com.dawidpiechaczek.apps.couchpotato.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dawidpiechaczek.apps.couchpotato.db.dao.DirectorDao
import com.dawidpiechaczek.apps.couchpotato.db.dao.FilmDao
import com.dawidpiechaczek.apps.couchpotato.db.dao.ReviewDao
import com.dawidpiechaczek.apps.couchpotato.db.entity.DirectorEntity
import com.dawidpiechaczek.apps.couchpotato.db.entity.FilmEntity
import com.dawidpiechaczek.apps.couchpotato.db.entity.ReviewEntity

@Database(
    entities = [
        FilmEntity::class,
        ReviewEntity::class,
        DirectorEntity::class
    ],
    exportSchema = false,
    version = 1
)
abstract class CouchDatabase : RoomDatabase() {
    abstract fun filmDao(): FilmDao
    abstract fun reviewDao(): ReviewDao
    abstract fun directorsDao(): DirectorDao
}