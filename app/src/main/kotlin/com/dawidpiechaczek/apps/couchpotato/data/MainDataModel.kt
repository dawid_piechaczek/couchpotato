package com.dawidpiechaczek.apps.couchpotato.data

import com.dawidpiechaczek.apps.couchpotato.db.dao.FilmDao
import com.dawidpiechaczek.apps.couchpotato.db.entity.FilmEntity
import com.dawidpiechaczek.apps.couchpotato.db.FilmsRepository
import com.dawidpiechaczek.apps.couchpotato.db.dao.DirectorDao
import com.dawidpiechaczek.apps.couchpotato.db.dao.ReviewDao
import com.dawidpiechaczek.apps.couchpotato.db.entity.DirectorEntity
import com.dawidpiechaczek.apps.couchpotato.db.entity.ReviewEntity
import io.reactivex.Observable

class MainDataModel(
    private val filmDao: FilmDao,
    private val directorDao: DirectorDao,
    private val reviewDao: ReviewDao
) : FilmsRepository {
    override fun deleteAllReviews(): Observable<Unit> {
        return Observable.fromCallable { reviewDao.deleteAll() }
    }

    override fun getFilmsWithIds(ids: List<Long>): Observable<List<FilmEntity>> {
        return filmDao.getFilmsWithIds(ids).toObservable()
    }

    override fun getAllReviews(): List<ReviewEntity> {
        return reviewDao.getAll()
    }

    override fun insertReview(reviewEntity: ReviewEntity): Observable<Unit> {
        return Observable.fromCallable { reviewDao.insert(reviewEntity) }
    }

    override fun insertDirectors(directorEntity: List<DirectorEntity>) {
        directorDao.insert(directorEntity)
    }

    override fun getDirectorById(id: Long): Observable<DirectorEntity> {
        return directorDao.getDirectorById(id).toObservable()
    }

    override fun insertFilm(filmEntity: List<FilmEntity>) {
        filmDao.insert(filmEntity)
    }

    override fun getAllFilms(): Observable<List<FilmEntity>> {
        return filmDao.getAll().toObservable()
    }
}