package com.dawidpiechaczek.apps.couchpotato.mapper

import com.dawidpiechaczek.apps.couchpotato.model.Similarity
import com.dawidpiechaczek.apps.couchpotato.model.SimilarityDTO

object SimilarityDTOToSimilarity {
    fun from(similarityDTO: SimilarityDTO) =
        Similarity(
            similarityDTO.id_film,
            similarityDTO.id_film2,
            similarityDTO.similarity
        )
}