package com.dawidpiechaczek.apps.couchpotato.ui.pager.page

import com.dawidpiechaczek.apps.couchpotato.db.FilmsRepository
import com.dawidpiechaczek.apps.couchpotato.db.entity.ReviewEntity
import com.dawidpiechaczek.apps.couchpotato.mapper.DirectorEntityToDirector
import com.dawidpiechaczek.apps.couchpotato.model.Director
import com.dawidpiechaczek.apps.couchpotato.ui.base.BaseViewModel
import com.dawidpiechaczek.apps.couchpotato.utils.RxSchedulers
import timber.log.Timber

class SlidePageFragmentViewModel(
    private val rxSchedulers: RxSchedulers,
    private val filmsRepository: FilmsRepository
) : BaseViewModel() {
    var id: Long? = null
    var title: String? = null
    var porsterUrl: String? = null
    var director: Director? = null
    var year: String? = null

    fun getDirectorById(id: Long) {
        addDisposable(
            filmsRepository.getDirectorById(id)
                .subscribeOn(rxSchedulers.network)
                .map { DirectorEntityToDirector.from(it) }
                .subscribe(
                    {
                        director = it
                    },
                    Timber::e
                )
        )
    }

    fun insertReview(reviewEntity: ReviewEntity) {
        addDisposable(
            filmsRepository.insertReview(reviewEntity)
                .subscribeOn(rxSchedulers.disk)
                .subscribe()
        )
    }
}