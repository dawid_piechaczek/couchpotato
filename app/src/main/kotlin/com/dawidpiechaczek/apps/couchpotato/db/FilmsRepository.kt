package com.dawidpiechaczek.apps.couchpotato.db

import com.dawidpiechaczek.apps.couchpotato.db.entity.DirectorEntity
import com.dawidpiechaczek.apps.couchpotato.db.entity.FilmEntity
import com.dawidpiechaczek.apps.couchpotato.db.entity.ReviewEntity
import io.reactivex.Observable

interface FilmsRepository {
    fun insertFilm(filmEntity: List<FilmEntity>)
    fun getAllFilms(): Observable<List<FilmEntity>>
    fun getFilmsWithIds(ids: List<Long>): Observable<List<FilmEntity>>

    fun insertDirectors(filmEntity: List<DirectorEntity>)
    fun getDirectorById(id: Long): Observable<DirectorEntity>

    fun insertReview(reviewEntity: ReviewEntity): Observable<Unit>
    fun getAllReviews(): List<ReviewEntity>
    fun deleteAllReviews(): Observable<Unit>
}