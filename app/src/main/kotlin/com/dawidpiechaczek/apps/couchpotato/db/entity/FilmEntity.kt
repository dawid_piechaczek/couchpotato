package com.dawidpiechaczek.apps.couchpotato.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "films")
data class FilmEntity(
    @PrimaryKey
    @ColumnInfo(name = "id_film")
    val id_film: Long,
    @ColumnInfo(name = "id_director")
    val id_director: Long,
    @ColumnInfo(name = "title_pl")
    val title_pl: String?,
    @ColumnInfo(name = "title_orgin")
    val title_orgin: String?,
    @ColumnInfo(name = "foto_mini")
    val foto_mini: String?,
    @ColumnInfo(name = "foto")
    val foto: String?,
    @ColumnInfo(name = "poster_mini")
    val poster_mini: String?,
    @ColumnInfo(name = "poster")
    val poster: String?,
    @ColumnInfo(name = "duration")
    val duration: Long,
    @ColumnInfo(name = "year_production")
    val year_production: String?,
    @ColumnInfo(name = "description_film")
    val description_film: String?,
    @ColumnInfo(name = "grade")
    val grade: Float,
    @ColumnInfo(name = "motto")
    val motto: String?,
    @ColumnInfo(name = "popularity")
    val popularity: Float,
    @ColumnInfo(name = "date_add")
    val date_add: String?,
    @ColumnInfo(name = "amount_grades")
    val amount_grades: Float,
    @ColumnInfo(name = "category")
    val category: String?
)