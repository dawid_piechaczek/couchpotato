package com.dawidpiechaczek.apps.couchpotato.model

data class DirectorDTO(
    val id_director: Long,
    val name_director: String
)