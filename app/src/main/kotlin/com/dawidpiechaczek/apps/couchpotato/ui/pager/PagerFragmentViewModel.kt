package com.dawidpiechaczek.apps.couchpotato.ui.pager

import com.dawidpiechaczek.apps.couchpotato.db.FilmsRepository
import com.dawidpiechaczek.apps.couchpotato.mapper.DirectorDTOToDirectorEntity
import com.dawidpiechaczek.apps.couchpotato.mapper.FilmDTOToFilmEntity
import com.dawidpiechaczek.apps.couchpotato.mapper.FilmEntityToFilm
import com.dawidpiechaczek.apps.couchpotato.model.Film
import com.dawidpiechaczek.apps.couchpotato.service.FilmService
import com.dawidpiechaczek.apps.couchpotato.ui.base.BaseViewModel
import com.dawidpiechaczek.apps.couchpotato.utils.RxSchedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.*

class PagerFragmentViewModel(
    private val rxSchedulers: RxSchedulers,
    private val filmService: FilmService,
    private val filmsRepository: FilmsRepository
) : BaseViewModel() {
    private val uiEvents = PublishSubject.create<List<Film>>()

    fun getUiEventsPublishSubject(): PublishSubject<List<Film>> {
        return uiEvents
    }

    fun deleteAllReviews() {
        addDisposable(
            filmsRepository.deleteAllReviews()
                .subscribeOn(rxSchedulers.disk)
                .subscribe()
        )
    }

    fun fetchFilms() {
        addDisposable(filmService.fetchFilms()
            .subscribeOn(rxSchedulers.network)
            .map { listOfFilms -> listOfFilms.map { FilmDTOToFilmEntity.from(it) } }
            .subscribe(
                {
                    filmsRepository.insertFilm(it)
                    Timber.d("Films are inserted")
                },
                {
                    Timber.e(it)
                }
            )
        )
    }

    fun fetchDirectors() {
        addDisposable(filmService.fetchDirectors()
            .subscribeOn(rxSchedulers.network)
            .map { listOfFilms -> listOfFilms.map { DirectorDTOToDirectorEntity.from(it) } }
            .subscribe(
                {
                    filmsRepository.insertDirectors(it)
                    Timber.d("Directors are inserted")
                },
                {
                    Timber.e(it)
                }
            )
        )
    }

    fun getTenRandomFilms() {
        val listOfFilmIds = getRandomNumberInRange(1, 2100)
        addDisposable(filmsRepository.getAllFilms()
            .subscribeOn(rxSchedulers.disk)
            .map { listOfFilms -> listOfFilms.map { FilmEntityToFilm.from(it) } }
            .subscribe(
                { listOfAllFilms ->
                    run {
                        val listToReturn = arrayListOf<Film>()
                        listOfFilmIds.forEach { listToReturn.add(listOfAllFilms[it]) }
                        uiEvents.onNext(listToReturn)
                    }
                },
                {
                    Timber.e(it)
                }
            )
        )
    }

    private fun getRandomNumberInRange(min: Int, max: Int): List<Int> {
        if (min >= max) {
            throw IllegalArgumentException("Max must be greater than min")
        }

        val r = Random()
        val list = arrayListOf<Int>()
        for (i in 0..19) {
            list.add(r.nextInt(max - min + 1) + min)
        }

        return list

    }
}