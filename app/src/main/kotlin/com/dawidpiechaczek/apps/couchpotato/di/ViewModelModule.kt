package com.dawidpiechaczek.apps.couchpotato.di

import com.dawidpiechaczek.apps.couchpotato.ui.pager.page.SlidePageFragmentViewModel
import com.dawidpiechaczek.apps.couchpotato.ui.main.MainActivityViewModel
import com.dawidpiechaczek.apps.couchpotato.ui.pager.PagerFragmentViewModel
import com.dawidpiechaczek.apps.couchpotato.ui.pager.summary.SummaryFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { PagerFragmentViewModel(get(), get(), get()) }
    viewModel { SlidePageFragmentViewModel(get(), get()) }
    viewModel { SummaryFragmentViewModel(get(), get(), get()) }
}