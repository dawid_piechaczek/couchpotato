package com.dawidpiechaczek.apps.couchpotato.model

class SimilarityDTO(
    var id_film: Long,
    var id_film2: Long,
    var similarity: Double
)