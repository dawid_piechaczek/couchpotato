package com.dawidpiechaczek.apps.couchpotato.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reviews")
data class ReviewEntity(
    @PrimaryKey
    @ColumnInfo(name = "id_film")
    val idFilm: Long,
    @ColumnInfo(name = "mark")
    val mark: Float?
)