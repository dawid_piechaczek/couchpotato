package com.dawidpiechaczek.apps.couchpotato.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dawidpiechaczek.apps.couchpotato.R
import com.dawidpiechaczek.apps.couchpotato.ui.pager.PagerFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val newFragment = PagerFragment.newInstance()
        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.fragment_container, newFragment).commit()
    }
}
