package com.dawidpiechaczek.apps.couchpotato.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dawidpiechaczek.apps.couchpotato.R
import com.dawidpiechaczek.apps.couchpotato.model.Film
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_recommendation_list.view.*

class RecommendAdapter : RecyclerView.Adapter<RecommendAdapter.ViewHolder>() {
    private val listOfFilms = arrayListOf<Film>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendAdapter.ViewHolder {
        val viewItem = LayoutInflater.from(parent.context).inflate(R.layout.item_recommendation_list, parent, false)
        return ViewHolder(viewItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(listOfFilms[position])

    override fun getItemCount() = listOfFilms.size

    fun setFilms(films: List<Film>) {
        listOfFilms.clear()
        listOfFilms.addAll(films)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(film: Film) = with(itemView) {
            item_film_title.text = film.title_pl
            Picasso.get()
                .load(setNewUrl(film.poster))
                .placeholder(R.drawable.cartoon_cinema)
                .into(item_film_poster)
        }

        private fun setNewUrl(oldUrl: String?): String {
            return oldUrl?.replace("d3gtl9l2a4fn1j.cloudfront.net", "image.tmdb.org") ?: ""
        }
    }
}