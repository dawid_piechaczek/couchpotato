package com.dawidpiechaczek.apps.couchpotato.app

import android.app.Application
import com.dawidpiechaczek.apps.couchpotato.di.*
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class CouchPotatoApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        Timber.plant(Timber.DebugTree())

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@CouchPotatoApp)
            modules(appModule, databaseModule, networkModule, dataModelModule, viewModelModule)
        }

    }
}
