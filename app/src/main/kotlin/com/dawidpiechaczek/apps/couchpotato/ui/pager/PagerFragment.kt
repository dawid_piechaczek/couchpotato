package com.dawidpiechaczek.apps.couchpotato.ui.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dawidpiechaczek.apps.couchpotato.R
import com.dawidpiechaczek.apps.couchpotato.adapter.SlidePageAdapter
import com.dawidpiechaczek.apps.couchpotato.utils.RxSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_pager.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class PagerFragment : Fragment() {
    private val pagerFragmentViewModel: PagerFragmentViewModel by viewModel()
    private val rxSchedulers: RxSchedulers by inject()
    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pager, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pagerAdapter = SlidePageAdapter(fragmentManager)
        films_viewpager.adapter = pagerAdapter
        pagerFragmentViewModel.deleteAllReviews()
        pagerFragmentViewModel.fetchFilms()
        pagerFragmentViewModel.fetchDirectors()

        disposables.addAll(pagerFragmentViewModel
            .getUiEventsPublishSubject()
            .observeOn(rxSchedulers.ui)
            .subscribeOn(rxSchedulers.network)
            .subscribe(
                {
                    pagerAdapter.setFilms(it)
                },
                {
                    Timber.e(it)
                }
            )
        )

        pagerFragmentViewModel.getTenRandomFilms()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    companion object {
        fun newInstance(): PagerFragment {
            return PagerFragment()
        }
    }
}