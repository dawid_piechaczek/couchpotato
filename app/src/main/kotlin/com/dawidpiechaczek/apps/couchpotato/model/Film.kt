package com.dawidpiechaczek.apps.couchpotato.model

class Film(
    var id_film: Long,
    var director: Long,
    var title_pl: String?,
    var poster: String?,
    var year_production: String?,
    var description_film: String?,
    var category: String?
)