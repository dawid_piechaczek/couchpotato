package com.dawidpiechaczek.apps.couchpotato.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.dawidpiechaczek.apps.couchpotato.model.Film
import com.dawidpiechaczek.apps.couchpotato.ui.pager.page.SlidePageFragment
import com.dawidpiechaczek.apps.couchpotato.ui.pager.summary.SummaryFragment

class SlidePageAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {
    private val listOfFilms = arrayListOf<Film>()

    fun setFilms(films: List<Film>) {
        listOfFilms.clear()
        listOfFilms.addAll(films)
        notifyDataSetChanged()
    }

    override fun getCount(): Int = listOfFilms.size

    override fun getItem(position: Int): Fragment =
        if (count-1 == position) {
            SummaryFragment.newInstance()
        } else {
            SlidePageFragment.newInstance(listOfFilms[position])
        }

}