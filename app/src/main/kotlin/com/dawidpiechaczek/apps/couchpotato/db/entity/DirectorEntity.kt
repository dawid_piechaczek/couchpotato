package com.dawidpiechaczek.apps.couchpotato.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "directors")
data class DirectorEntity(
    @PrimaryKey
    @ColumnInfo(name = "id_director")
    val idDirector: Long,
    @ColumnInfo(name = "name_director")
    val nameDirector: String
)