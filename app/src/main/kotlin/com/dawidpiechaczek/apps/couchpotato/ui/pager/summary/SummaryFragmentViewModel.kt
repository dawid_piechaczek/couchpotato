package com.dawidpiechaczek.apps.couchpotato.ui.pager.summary

import com.dawidpiechaczek.apps.couchpotato.db.FilmsRepository
import com.dawidpiechaczek.apps.couchpotato.mapper.FilmEntityToFilm
import com.dawidpiechaczek.apps.couchpotato.mapper.ReviewEntityToReview
import com.dawidpiechaczek.apps.couchpotato.mapper.SimilarityDTOToSimilarity
import com.dawidpiechaczek.apps.couchpotato.model.Film
import com.dawidpiechaczek.apps.couchpotato.model.FilmIds
import com.dawidpiechaczek.apps.couchpotato.model.Review
import com.dawidpiechaczek.apps.couchpotato.service.FilmService
import com.dawidpiechaczek.apps.couchpotato.ui.base.BaseViewModel
import com.dawidpiechaczek.apps.couchpotato.utils.RxSchedulers
import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

class SummaryFragmentViewModel(
    private val rxSchedulers: RxSchedulers,
    private val filmsRepository: FilmsRepository,
    private val filmService: FilmService
) : BaseViewModel() {
    private val uiEvents = PublishSubject.create<List<Film>>()

    fun getUiEventsPublishSubject(): PublishSubject<List<Film>> {
        return uiEvents
    }

    fun getReviewsForFilms() {
        addDisposable(
            getReviews()
                .subscribeOn(rxSchedulers.disk)
                .observeOn(rxSchedulers.ui)
                .subscribe({
                    Timber.d("Reviews fetched")
                }, Timber::e)
        )
    }

    private fun getFilmsForIds(ids: List<Long>) {
        addDisposable(
            filmsRepository.getFilmsWithIds(ids)
                .subscribeOn(rxSchedulers.disk)
                .map { listOfFilms -> listOfFilms.map { FilmEntityToFilm.from(it) } }
                .subscribe({
                    uiEvents.onNext(it)
                }, Timber::e)
        )
    }

    private fun getRecommendedFilms(filmIds: List<Long>) {
        addDisposable(
            filmService.fetchSimilarFilms(FilmIds(filmIds))
                .subscribeOn(rxSchedulers.network)
                .map { listOfSimilarities -> listOfSimilarities.map { SimilarityDTOToSimilarity.from(it) } }
                .subscribe(
                    {
                        val list = arrayListOf<Long>()
                        it.forEach { list.add(it.id_film2) }
                        getFilmsForIds(list)
                    },
                    Timber::e
                )
        )
    }

    private fun getReviews(): Completable {
        return Completable.fromCallable {
            val filmReviews: List<Review> = filmsRepository.getAllReviews().map { ReviewEntityToReview.from(it) }
            val list = arrayListOf<Long>()
            filmReviews.forEach { list.add(it.idFilm) }
            getRecommendedFilms(list)
        }
    }
}