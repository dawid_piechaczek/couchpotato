package com.dawidpiechaczek.apps.couchpotato.utils

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RxSchedulers(
    var network: Scheduler? = Schedulers.io(),
    var computation: Scheduler? = Schedulers.computation(),
    var disk: Scheduler? = Schedulers.newThread(),
    var ui: io.reactivex.Scheduler = AndroidSchedulers.mainThread()
    )