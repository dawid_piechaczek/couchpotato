package com.dawidpiechaczek.apps.couchpotato.di

import com.dawidpiechaczek.apps.couchpotato.utils.RxSchedulers
import org.koin.dsl.module

val appModule = module {
    single { RxSchedulers() }
}