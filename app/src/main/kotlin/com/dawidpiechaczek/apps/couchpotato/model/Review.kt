package com.dawidpiechaczek.apps.couchpotato.model

class Review(
    val idFilm: Long,
    val mark: Float?
)