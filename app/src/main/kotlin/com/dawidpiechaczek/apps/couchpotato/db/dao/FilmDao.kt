package com.dawidpiechaczek.apps.couchpotato.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dawidpiechaczek.apps.couchpotato.db.entity.FilmEntity
import io.reactivex.Single

@Dao
interface FilmDao {

    @Query(GET_ALL_FILMS)
    fun getAll(): Single<List<FilmEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(film: List<FilmEntity>)

    @Query(GET_FILMS_WITH_IDS)
    fun getFilmsWithIds(ids: List<Long>): Single<List<FilmEntity>>

    companion object {
        const val GET_ALL_FILMS: String = "SELECT * FROM films WHERE year_production >= '1990'"
        const val GET_FILMS_WITH_IDS: String = "SELECT * FROM films WHERE id_film IN (:ids)"
    }
}