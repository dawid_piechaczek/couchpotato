package com.dawidpiechaczek.apps.couchpotato.ui.pager.page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.dawidpiechaczek.apps.couchpotato.R
import com.dawidpiechaczek.apps.couchpotato.db.entity.ReviewEntity
import com.dawidpiechaczek.apps.couchpotato.model.Film
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.slide_page_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class SlidePageFragment() : Fragment() {
    private val slidePageFragmentViewModel: SlidePageFragmentViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.slide_page_fragment, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setArgumentsForFilm()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFieldsOnView()
    }

    private fun setFieldsOnView() {
        film_title_text.text = slidePageFragmentViewModel.title
        film_director_text.text = "Reżyseria: ${slidePageFragmentViewModel.director?.name_director}"
        film_year_text.text = "Rok: ${setFormattedDateTime(slidePageFragmentViewModel.year)}"
        film_vote_yes_button.setOnClickListener {
            if (isRatingChoosen()) {
                slidePageFragmentViewModel.insertReview(
                    ReviewEntity(
                        slidePageFragmentViewModel.id?:0,
                        film_grade_rating.rating)
                )
                Toast.makeText(context, "Dodano ocenę", Toast.LENGTH_LONG).show()
            } else {
                slidePageFragmentViewModel.insertReview(
                    ReviewEntity(
                        slidePageFragmentViewModel.id?:0,
                        null)
                )
                Toast.makeText(context, "Oznaczono jako pominięte", Toast.LENGTH_LONG).show()
            }
        }
        Picasso.get()
            .load(setNewUrl(slidePageFragmentViewModel.porsterUrl))
            .placeholder(R.drawable.cartoon_cinema)
            .into(film_poster_image)
    }

    private fun isRatingChoosen(): Boolean {
        return film_grade_rating.rating != 0f
    }

    private fun setFormattedDateTime(date: String?): Int {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val calendar = Calendar.getInstance()
        calendar.time = simpleDateFormat.parse(date)
        return calendar.get(Calendar.YEAR)
    }

    private fun setArgumentsForFilm() {
        slidePageFragmentViewModel.getDirectorById(arguments?.getLong("director", 0L) ?: 0L)
        slidePageFragmentViewModel.id = arguments?.getLong("id", 0L)
        slidePageFragmentViewModel.title = arguments?.getString("title", "")
        slidePageFragmentViewModel.porsterUrl = arguments?.getString("posterUrl", "")
        slidePageFragmentViewModel.year = arguments?.getString("year", "")
    }

    private fun setNewUrl(oldUrl: String?): String {
        return oldUrl?.replace("d3gtl9l2a4fn1j.cloudfront.net", "image.tmdb.org") ?: ""
    }

    companion object {
        fun newInstance(film: Film): SlidePageFragment {
            val fragmentFirst = SlidePageFragment()
            val args = Bundle()
            args.putLong("id", film.id_film)
            args.putString("title", film.title_pl)
            args.putString("posterUrl", film.poster)
            args.putLong("director", film.director)
            args.putString("year", film.year_production)
            fragmentFirst.arguments = args
            return fragmentFirst
        }
    }


}