package com.dawidpiechaczek.apps.couchpotato.model

class FilmDTO(
    var id_film: Long,
    var id_director: Long,
    var title_pl: String?,
    var title_orgin: String?,
    var foto_mini: String?,
    var foto: String?,
    var poster_mini: String?,
    var poster: String?,
    var duration: Long,
    var year_production: String?,
    var description_film: String?,
    var grade: Float,
    var motto: String?,
    var popularity: Float,
    var date_add: String?,
    var amount_grades: Float,
    var category: String?
)